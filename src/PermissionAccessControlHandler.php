<?php

namespace Drupal\permission_ui;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the user permission entity type.
 */
class PermissionAccessControlHandler extends EntityAccessControlHandler {

}
